import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BusinessService } from '../business.service';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.css']
})
export class AuthenticateComponent implements OnInit {
  loginForm:FormGroup;
  register1Form:FormGroup;
  registerFormOpen=false;
  logindata:any;
  message:String;
  isMassage :Boolean;
  constructor(private fb: FormBuilder, private bs: BusinessService,private router: Router) { 
    this.LoginForm();
    this.registerForm();
  }


  register(){
    this.registerFormOpen=true;
  }
  loginUser(){
    this.registerFormOpen=false;
  }
  LoginForm() {
    this.loginForm = this.fb.group({
      staff_Id:['',Validators.required],
      password: ['', Validators.required ]
    });
  }
  registerForm() {
    this.register1Form = this.fb.group({
      staff_Id:['',Validators.required],
      password: ['', Validators.required ]
    });
  }
 
  LogIn(staff_Id,password){
    this.bs.logInUser(staff_Id,password).subscribe(res=>{
      console.log("Response received: ",res);
      this.logindata = res;
      if(this.logindata.status===false){
        this.message =this.logindata.message;
        this.isMassage=true;
      }else{
        this.isMassage=false;
        console.log(this.logindata.token);
        localStorage.setItem('x-access-token', JSON.stringify(this.logindata.token.token));
        localStorage.setItem('isAdmin',JSON.stringify(this.logindata.token.Designation));
        localStorage.setItem('user',JSON.stringify(this.logindata.token.username));
        localStorage.setItem('_id',JSON.stringify(this.logindata.token.id));
        this.router.navigateByUrl('/business/get');
      }

   })
    //console.log("Loged in data: ",this.logedindata);
  }
  // logIn(staff_Id,password){
  //   this.bs.logInUser(staff_Id,password)
  // }

  Register(staff_Id,password){
    console.log("stgg data is: ",staff_Id ,"and: ",password);
   this.bs.registerUser(staff_Id,password).subscribe(res=>{
      console.log("regisred in data: ",res);
      this.logindata = res;
      if( this.logindata.status===false){
        this.message =this.logindata.message;
        this.isMassage=true;
      }else{
        this.isMassage=false;
        console.log(this.logindata.token);
        this.registerFormOpen=false;
      
      }

    });
   
  }
  ngOnInit() {

  }

}
