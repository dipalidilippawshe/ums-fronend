import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {
  uri = 'http://localhost:3000/user';
  constructor(private http: HttpClient) { }

  logInUser(staffId, pwd){
    var object = {
      staff_Id:staffId,
      password:pwd
    }
    return this.http.post(`${this.uri}/login`, object)
   // .subscribe(res => console.log('Done'));
    

  }
  registerUser(staffId, pwd){
    var object = {
      staff_Id:staffId,
      password:pwd
    }
    return this.http.post(`${this.uri}/register`, object)
   // .subscribe(res => console.log('Done'));
    

  }

  addBusiness(person_name, business_name, business_gst_number) {
    const obj = {
      person_name: person_name,
      business_name: business_name,
      business_gst_number: business_gst_number
    };
    console.log(obj);
    this.http.post(`${this.uri}/add`, obj)
        .subscribe(res => console.log('Done'));
  }
  getBusinesses() {
    return this
           .http
           .get(`${this.uri}`);
  }

  getUsers(){
    return this
           .http
           .get(`${this.uri}`);
  }
  addUser(user){
    this.http.post(`${this.uri}/adduser`, user)
    .subscribe(res => console.log('Done'));
  }

  getUserById(id){
    return this
           .http
           .get(`${this.uri}/getuserById/`+id);
  }

  updateUser(id,user){
    return this.http.put(`${this.uri}/updateuser/`+id,user);
  }
}
