import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import User from '../User';
import { BusinessService } from '../business.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-gst-get',
  templateUrl: './gst-get.component.html',
  styleUrls: ['./gst-get.component.css']
})
export class GstGetComponent implements OnInit {
  users: User[];
  constructor(private bs: BusinessService,private router: Router) { }
  isAdmin = localStorage.getItem('isAdmin');
  userName = localStorage.getItem("user")
  ID = localStorage.getItem("_id");
  user:any;
  itsAdmin = false;
  ngOnInit() {
    console.log("IS ADMIN IS :",this.isAdmin);
    this.isAdmin=JSON.parse(this.isAdmin);
    if(this.isAdmin){
console.log("isadmin checks");
      this.bs
      .getUsers()
      .subscribe((data: User[]) => {
        this.users = data;
    });
   this.itsAdmin=true;
    }else{
      console.log("Id is: ",this.ID);
      this.ID=JSON.parse(this.ID);
      this.bs.getUserById(this.ID)
    .subscribe((data: User) => {
      this.user = data;
      console.log("this.user: ",this.user);
     }); 
     this.itsAdmin=false;

    }
   
    // this.bs
    //   .getUsers()
    //   .subscribe((data: User[]) => {
    //     this.users = data;
    // });
  }
  gotoAdd(){
    this.router.navigateByUrl('business/create');
  }
  editUser(user){
    this.router.navigateByUrl('business/edit/'+user._id);
  }
  logOut(){
    localStorage.removeItem("x-access-token");
    localStorage.removeItem("isAdmin");
    localStorage.removeItem("_id");
    localStorage.removeItem("user");
 this.router.navigateByUrl('login');
  }
}
