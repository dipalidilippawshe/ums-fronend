import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{ GstAddComponent} from './gst-add/gst-add.component';
import { GstGetComponent} from './gst-get/gst-get.component';
import { GstEditComponent} from './gst-edit/gst-edit.component';
import {AuthenticateComponent} from './authenticate/authenticate.component';

const routes: Routes = [
  {
    path:'business/create',
    component:GstAddComponent
  },
  {
    path:'business/get',
    component:GstGetComponent
  },
  {
    path:'business/edit/:id',
    component:GstEditComponent,
    data:{title:'Edit Product'}
  },
  {
    path:'login',
    component:AuthenticateComponent,
    data:{title:'login'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
