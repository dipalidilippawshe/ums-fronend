import { Component } from '@angular/core';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import {
  NavigationCancel,
  Event,NavigationEnd,
  NavigationError,NavigationStart,Router} from '@angular/router'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  title = 'angular7crud';
  token = localStorage.getItem('x-access-token');
  isAdmin = localStorage.getItem('isAdmin');
  LogedIn=false;
  constructor(private _loadingBar: SlimLoadingBarService, private _router: Router) {
    console.log("token is: ",this.token);
    console.log("title: ",this.title);
    if(this.token && this.isAdmin){
      this.LogedIn = true;
      this._router.navigateByUrl('/business/get');
    }else{
      this._router.navigateByUrl('login');
    }
    this._router.events.subscribe((event: Event) => {
      this.navigationInterceptor(event);
    });
  }
  private navigationInterceptor(event: Event): void {
    if (event instanceof NavigationStart) {
      this._loadingBar.start();
    }
    if (event instanceof NavigationEnd) {
      this._loadingBar.complete();
    }
    if (event instanceof NavigationCancel) {
      this._loadingBar.stop();
    }
    if (event instanceof NavigationError) {
      this._loadingBar.stop();
    }
  }
}
