import { Component, OnInit } from '@angular/core';
import User from '../User';
import { BusinessService } from '../business.service';
import {Router, ActivatedRoute, Params, Data} from '@angular/router';
import { FormGroup,FormBuilder,  Validators } from '@angular/forms';
@Component({
  selector: 'app-gst-edit',
  templateUrl: './gst-edit.component.html',
  styleUrls: ['./gst-edit.component.css']
})
export class GstEditComponent implements OnInit {
  id:any;
  user: any;
  editForm1:FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(private fb: FormBuilder,private bs: BusinessService,private router: ActivatedRoute,private route:Router) { 
    this.editForm();
   this.callMe();
  }
  ngOnInit() {
    this.id =   this.router.snapshot.params['id'];
    console.log(this.id);//undefined
    this.bs.getUserById(this.id)
    .subscribe((data: User) => {
      this.user = data;
     this.inputData();
      console.log("this.user: ",this.user);
  });
  }
  editForm() {
    console.log("@ edit form: ",this.user);
    this.editForm1 = this.fb.group({
      person_name: ['', Validators.required ],
      person_email: ['', Validators.required ,Validators.pattern(this.emailPattern)],
      staff_Id:['',Validators.required],
      person_ContactNo: ['', Validators.required ],
      person_address:[''],
      Person_dob:['',Validators.required],
      gender:['',Validators.required],

    });
  }
  inputData(){
    this.editForm1 = this.fb.group({
      person_name: this.user.data.person_name,
      person_email: this.user.data.person_email,
      staff_Id:this.user.data.staff_Id,
      person_address:this.user.data.person_address,
      

    });
  }

  

  callMe(){
    this.ngOnInit();
  }

  goBack(){
    this.route.navigateByUrl('business/get');
  }

  updateUser(name, email, staffId,address){
    let user={
      person_name: name,
      person_email:email,
      staff_Id:staffId,
      person_address:address
    }
    this.bs.updateUser(this.id,user).subscribe((data: User) => {
      this.user = data;
    
      console.log("this.user: ",this.user);
     }); 
     this.route.navigateByUrl('business/get');
  }


}
