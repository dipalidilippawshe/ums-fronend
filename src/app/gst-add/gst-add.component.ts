import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BusinessService } from '../business.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-gst-add',
  templateUrl: './gst-add.component.html',
  styleUrls: ['./gst-add.component.css']
})
export class GstAddComponent implements OnInit {
  angForm: FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  //phoneNumber = "([0-9]|[0-9]|[0-9])";
  //constructor() { }
  constructor(private fb: FormBuilder, private bs: BusinessService,private router: Router) {
    this.createForm();
  }
  createForm() {
    this.angForm = this.fb.group({
      person_name: ['', Validators.required ],
      person_email: ['', Validators.required ,Validators.pattern(this.emailPattern)],
      staff_Id:['',Validators.required],
      person_ContactNo: ['', Validators.required ],
      person_address:[''],
      Person_dob:['',Validators.required],
      gender:['',Validators.required],

    });
  }

  addBusiness(person_name, busines_name, business_gst_number) {
    console.log("INADD BUSINESS");
    this.bs.addBusiness(person_name, busines_name, business_gst_number);
  }

  addUser(name, email, staffId,address) {
    console.log("INADD BUSINESS");
    let user={
      person_name: name,
      person_email:email,
      staff_Id:staffId,
      person_address:address
    }
    this.bs.addUser(user);
    this.router.navigateByUrl('business/get');

  }

  ngOnInit() {
  }

  goBack(){
    this.router.navigateByUrl('business/get');
  }

}
